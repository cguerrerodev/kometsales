import React from 'react';
import './App.css';
import {BrowserRouter} from 'react-router-dom'; 
import Header from './components/main/Header';
import Menu from './components/main/Menu';
import Content from './components/main/Content';


function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Header />
        <Menu />
        <Content />
      </BrowserRouter>
    </div>
  );
}

export default App;
