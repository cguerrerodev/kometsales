import React, { Component } from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import Seller from '../sellers/Seller';


class SellersList extends Component {

  
  state = {
    sellers: []
  }

  
  showSeller(sellerId){
  /*  if(this.sellerDetail === undefined){
      this.sellerDetail = ReactDOM.render(<Seller sellerId = {sellerId}/>, document.getElementById('sellerCointainer'));
    }*/
  }

  componentDidMount(){
    axios.get('http://localhost:8080/seller')
      .then(res => {
        console.log(res);
        this.setState({
          sellers: res.data.slice(0,10)
        });
      })
  }

  render() {

    if (this.state.sellers) {

      return (
        <div>
          <div className="page-header">
            <h3>Sellers
            </h3>          
          </div>
          <div className="bs-component column" >
            <table className="table table-hover">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">E-Mail</th>
              </tr>


              {this.state.sellers.map(seller => {

                return (
                  <tr className="table-active">
                    <td>
                      <a onClick={this.showSeller}  >
                        {seller.id}
                      </a>
                    </td>
                    <td>
                      <a onClick={this.showSeller}  >
                        {seller.name}
                      </a>
                    </td>
                    <td>
                      {seller.eMail}
                    </td>
                  </tr>);
                }
              )
              }
            </table>
          </div>

          <div id= "sellerCointainer"  className="bs-component column" >
            
          </div>
        
        </div>
      );
    } else {

      return (
        <div>
            No sellers
        </div>
      );

    }

  }

}


export default SellersList;