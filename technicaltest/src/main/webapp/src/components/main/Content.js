import React from 'react';
import SellersList from '../sellers/SellersList';
import Seller from '../sellers/Seller';
import SellsFileLoader from '../sells/SellsFileLoader';
import { Route } from 'react-router-dom';

function Content() {
  return (

    <div className="container">
          <SellersList/>         
          <SellsFileLoader />
    </div>
  );
}

export default Content;