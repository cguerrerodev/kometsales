package com.kometsales.technicaltest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kometsales.technicaltest.business.SellManager;
import com.kometsales.technicaltest.business.exception.TechnicalTestException;

@RestController
@CrossOrigin("*")
public class SellsController {
	
	@Autowired
	private SellManager sellManager;
	
	@PostMapping("/sells/fileupload")
    public void uploadFile(@RequestParam("file") MultipartFile file) {
        try {
			
        	sellManager.loadSellsFromCSVFile(file);
			
		} catch (TechnicalTestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
