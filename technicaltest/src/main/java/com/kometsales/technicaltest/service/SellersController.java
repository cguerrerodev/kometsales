package com.kometsales.technicaltest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.kometsales.technicaltest.business.SellerManager;
import com.kometsales.technicaltest.business.dto.SellerDTO;

@RestController
@CrossOrigin("*")
public class SellersController {

	@Autowired
	private SellerManager sellerManager;
	
	@GetMapping("seller")
	public List<SellerDTO> getSellers(){
		
		return sellerManager.getSellers();
		
	}

	@GetMapping("seller/{id}")
	public SellerDTO getSellerById(@PathVariable("id") long id){
		
		return sellerManager.getSeller(id);
		
	}

	
}
