package com.kometsales.technicaltest.persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.kometsales.technicaltest.persistence.entity.SellEntity;

@Component
public class SellRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void save(SellEntity sellEntity) {
		
		jdbcTemplate.update("INSERT INTO sell (sellerid, product, amount, date) "
				+ "values (?,?,?,?)", 
				sellEntity.getSellerId(),
				sellEntity.getProduct(),
				sellEntity.getAmount(),
				new java.sql.Date(sellEntity.getDate().getTime()));
		
	}

}
