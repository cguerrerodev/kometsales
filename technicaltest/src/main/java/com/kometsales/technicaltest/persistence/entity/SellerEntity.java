package com.kometsales.technicaltest.persistence.entity;


public class SellerEntity {
	
	private long id;
	
	private String name;
	
	private String eMail;
	
	private double commissionPercentage;
	
	public SellerEntity() {}

	public SellerEntity(long id, String name, String eMail, double commissionPercentage) {
		
		this.id = id;
		this.name = name;
		this.eMail = eMail;
		this.commissionPercentage = commissionPercentage;
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public double getCommissionPercentage() {
		return commissionPercentage;
	}

	public void setCommissionPercentage(double commissionPercentage) {
		this.commissionPercentage = commissionPercentage;
	}
	
	

}
