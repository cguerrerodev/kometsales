package com.kometsales.technicaltest.persistence.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.kometsales.technicaltest.persistence.entity.SellerEntity;

@Component
public class SellerRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<SellerEntity> getSellers(){
		  
		return jdbcTemplate.query(
	            "SELECT id, name, email, commission FROM seller", 
	            (rs, rowNum) -> new SellerEntity(rs.getLong("id"), rs.getString("name"), 
	            						rs.getString("email"), rs.getDouble("commission")));
	        
	}


	public SellerEntity getSeller(long id) {

		List<SellerEntity> list = jdbcTemplate.query(
	            "SELECT id, name, email, commission FROM seller where id = ?", new Long[] { id },
	            (rs, rowNum) -> new SellerEntity(rs.getLong("id"), rs.getString("name"), 
	            						rs.getString("email"), rs.getDouble("commission")));
	      	
		return list.isEmpty() ? null : list.get(0);
	}

}
