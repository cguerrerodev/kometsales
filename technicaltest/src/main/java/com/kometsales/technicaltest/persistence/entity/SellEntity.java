package com.kometsales.technicaltest.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import com.opencsv.bean.CsvBindByName;

public class SellEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	private long sellerId;
	
	private String product;
	
	private double amount;
	
	private double commission;
	
	private Date date;

	public SellEntity(long id, long sellerId, String product, double amount, Date date) {
		super();
		this.id = id;
		this.sellerId = sellerId;
		this.product = product;
		this.amount = amount;
		this.date = date;
	}
	
	public SellEntity() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	

	
	
}
