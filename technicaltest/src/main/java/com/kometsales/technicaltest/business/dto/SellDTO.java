package com.kometsales.technicaltest.business.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

public class SellDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	@CsvBindByName(column="sellerid")
	@Positive
	private long sellerId;
	
	@CsvBindByName
	@NotNull
	@Size(min=2, max=50)
	private String product;
	
	@CsvBindByName
	@PositiveOrZero
	private double amount;

	@CsvBindByName
	@PositiveOrZero
	private double commission;
	
	@CsvBindByName
	@CsvDate(value = "yyyy-MM-dd")
	@NotNull
	@PastOrPresent
	private Date date;

	public SellDTO(long sellerId, String product, double amount, Date date) {
		super();
		this.sellerId = sellerId;
		this.product = product;
		this.amount = amount;
		this.date = date;
	}
	
	public SellDTO() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	

	
	
}
