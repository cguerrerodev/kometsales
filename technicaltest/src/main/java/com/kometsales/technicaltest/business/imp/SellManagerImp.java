package com.kometsales.technicaltest.business.imp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.kometsales.technicaltest.business.SellManager;
import com.kometsales.technicaltest.business.dto.SellDTO;
import com.kometsales.technicaltest.business.exception.TechnicalTestException;
import com.kometsales.technicaltest.persistence.entity.SellEntity;
import com.kometsales.technicaltest.persistence.entity.SellerEntity;
import com.kometsales.technicaltest.persistence.repository.SellRepository;
import com.kometsales.technicaltest.persistence.repository.SellerRepository;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Component
@EnableAsync
public class SellManagerImp implements SellManager {

	@Autowired
	private SellRepository sellRepository;
	
	@Autowired
	private SellerRepository sellerRepository;

	@Async
	public void loadSellsFromCSVFile(MultipartFile file) throws TechnicalTestException {

		if (file.isEmpty()) {

			throw new TechnicalTestException("Empty file");

		} else {

			Reader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
			} catch (IOException e) {

				throw new TechnicalTestException("Error reading file", e);
			}

			CsvToBean<SellDTO> csvToBean = new CsvToBeanBuilder(reader).withType(SellDTO.class)
					.withIgnoreLeadingWhiteSpace(true).build();

			List<SellDTO> sells = csvToBean.parse();

			for (SellDTO sellDTO : sells) {
				
				SellEntity sellEntity = new SellEntity();
				BeanUtils.copyProperties(sellDTO, sellEntity);
				
				double commission = calculateCommission(sellEntity);
				sellEntity.setCommission(commission);
				
				sellRepository.save(sellEntity);
				
			}
			
		}
	}

	@Override
	public double calculateCommission(SellEntity sellEntity) {
		
		SellerEntity sellerEntity = sellerRepository.getSeller(sellEntity.getSellerId());
		return sellEntity.getAmount() * (sellerEntity.getCommissionPercentage() / 100);
		
	}

	@Override
	public void save(SellDTO sellDTO) throws TechnicalTestException {

		SellEntity sellEntity = new SellEntity();

		BeanUtils.copyProperties(sellDTO, sellEntity);

		sellRepository.save(sellEntity);

	}

}
