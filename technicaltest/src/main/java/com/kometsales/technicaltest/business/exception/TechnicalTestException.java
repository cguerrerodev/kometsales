package com.kometsales.technicaltest.business.exception;

public class TechnicalTestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	public TechnicalTestException( String message) {
		
		super(message);
		
	}

	public TechnicalTestException( String message, Exception cause) {
		
		super(message, cause);
		
	}

	
}
