package com.kometsales.technicaltest.business.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kometsales.technicaltest.business.SellerManager;
import com.kometsales.technicaltest.business.dto.SellerDTO;
import com.kometsales.technicaltest.persistence.entity.SellerEntity;
import com.kometsales.technicaltest.persistence.repository.SellerRepository;

@Component
public class SellerManagerImp implements SellerManager{

	@Autowired
	private SellerRepository sellerRepository;
	
	@Override
	public List<SellerDTO> getSellers() {

		List <SellerDTO> sellers = new ArrayList<SellerDTO>();
		
		for (SellerEntity sellerEntity : sellerRepository.getSellers()) {
			SellerDTO sellerDTO = new SellerDTO();
			BeanUtils.copyProperties(sellerEntity, sellerDTO);
			sellers.add(sellerDTO);
		}
		
		return sellers;
	}

	@Override
	public SellerDTO getSeller(long id) {
		
		SellerEntity sellerEntity = sellerRepository.getSeller(id);
		
		if(sellerEntity == null) {
			return null;
		}

		SellerDTO sellerDTO = new SellerDTO();
		BeanUtils.copyProperties(sellerEntity, sellerDTO);
		return sellerDTO;
		
	}

	
}
