package com.kometsales.technicaltest.business;

import org.springframework.web.multipart.MultipartFile;

import com.kometsales.technicaltest.business.dto.SellDTO;
import com.kometsales.technicaltest.business.exception.TechnicalTestException;
import com.kometsales.technicaltest.persistence.entity.SellEntity;

public interface SellManager {
	
	void loadSellsFromCSVFile(MultipartFile file) throws TechnicalTestException;

	void save(SellDTO sellDTO) throws TechnicalTestException;

	double calculateCommission(SellEntity sellEntity);
	
}
