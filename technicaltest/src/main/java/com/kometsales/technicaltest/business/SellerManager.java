package com.kometsales.technicaltest.business;

import java.util.List;

import com.kometsales.technicaltest.business.dto.SellerDTO;

public interface SellerManager {
	
	public List<SellerDTO> getSellers();
	
	public SellerDTO getSeller(long id);

}
