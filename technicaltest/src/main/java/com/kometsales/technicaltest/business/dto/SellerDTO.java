package com.kometsales.technicaltest.business.dto;

import java.io.Serializable;

public class SellerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String name; 
	
	private String eMail;
	
	private double commissionPercentage; 

	private double currentCommissions;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public double getCommissionPercentage() {
		return commissionPercentage;
	}

	public void setCommissionPercentage(double commissionPercentage) {
		this.commissionPercentage = commissionPercentage;
	}

	public double getCurrentCommissions() {
		return currentCommissions;
	}

	public void setCurrentCommissions(double currentCommissions) {
		this.currentCommissions = currentCommissions;
	}
	
	
	
}
