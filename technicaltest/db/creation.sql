
create database kometsales_test;

create user 'testuser'@'%' identified by 'testpassword';

grant all on kometsales_test.* to 'testuser'@'%';

use kometsales_test;

create table seller(  
	id 						int(10)	 UNSIGNED PRIMARY KEY, 
	name 					varchar(30) not null,  
 	email 					varchar(50),
 	commission				float
);  

create table sell(  

	id 						int(10)	 UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	sellerid 				int(10)  UNSIGNED,  
 	product 				varchar(50) not null,
 	amount					float,
 	commission				float,
 	date					datetime,
 	
    FOREIGN KEY (sellerid)  REFERENCES seller(id)
);  
