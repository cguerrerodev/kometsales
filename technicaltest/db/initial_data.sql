

insert into seller (id, name, email, commission) values (1, 'Eduardo Guillen', 'eguillen@gmail.com', 3.0);
insert into seller (id, name, email, commission) values (2, 'José Ortiz', 'jortiz@gmail.com', 2.1);
insert into seller (id, name, email, commission) values (3, 'Camilo Restrepo', 'crestrepo@gmail.com', 1.95);
insert into seller (id, name, email, commission) values (4, 'Pablo Tortolero', 'ptortolero@gmail.com', 0.33);